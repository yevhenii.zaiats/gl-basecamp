#include "func.h"

int main()
{
//Variables
    int sock;
    char buf[MAXLEN];
    int numbites;
    struct addrinfo hints, *servinfo;
    char s[INET6_ADDRSTRLEN];


    //Set up struck info
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    //Use getaddrinfo
    if((getaddrinfo("127.0.0.1",PORT,&hints,&servinfo))!= 0)
    {
        printf("Error: getaddrinfo()\n");
        return -1;
    }
    //Create socket
        if((sock = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol)) == -1)
        {
            printf("Error: soket()\n");
            return -1;
        }
        //Connect
        if((connect(sock,servinfo->ai_addr,servinfo->ai_addrlen)) == -1)
        {
            printf("Error: connect\n");
            return -1;
        }
        inet_ntop(servinfo->ai_family, get_in_addr((struct sockaddr*)servinfo->ai_addr), s, sizeof(s));
    printf("Client connected to %s \n", s);
    
        memset(buf, 0, MAXLEN);
        char mssg[MAXLEN];
        //recieve massage
        if((numbites = recv(sock,buf,MAXLEN,0))==-1)
        {
            printf("Error: recv()\n");
            return -1;
        }
        printf("%s \n", buf);
        sleep(10);
        if((send(sock, "Succesfull connection!", sizeof("Succesfull connection!"), 0)) == -1)
        {
            printf("Error: send()\n");
            return -1;
        }
    close(sock);
    return 0;
}