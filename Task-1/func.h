#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>


#define PORT "53000"
#define MAXLEN 100
#define BACKLOG 10

void *get_in_addr(struct sockaddr *sa)
{
 if (sa->sa_family == AF_INET) {
 return &(((struct sockaddr_in*)sa)->sin_addr);
 }
 return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
 //Fill struct data
void fill_data(struct addrinfo *hints, struct addrinfo *servinfo)
{
    memset(&hints, 0, sizeof(hints));
    hints->ai_family = AF_UNSPEC;
    hints->ai_socktype = SOCK_STREAM;
    hints->ai_flags = AI_PASSIVE;
    if((getaddrinfo(NULL,PORT,&hints,&servinfo)) != 0)
    {
        printf("Error : getadrrinfo()\n");
    }
}
//Create socket
void start(int *sock, struct addrinfo *servinfo, int *yes)
{
    if((sock = socket(servinfo->ai_family,servinfo->ai_socktype, servinfo->ai_protocol))==-1)
    {
        printf("Error: socket()\n");
    }
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        {
            printf("Error: setsockopt\n");
        }
    //Bind socket
    if((bind(sock, servinfo->ai_addr, servinfo->ai_addrlen)) == -1)
    {
        close(sock);
        printf("Error: bind()\n");
    }
    freeaddrinfo(servinfo);
}

void wain_client(int *sock,socklen_t *sin_size, struct sockaddr_storage *their_addr)
{
    //Listening
    if((listen(*sock,BACKLOG)) == -1)
    {
        printf("Error: Listen()\n");
        return -1;
    }
    printf("Server waiting for connection ... ... ...\n");
    //Accept
    *sin_size = sizeof(their_addr);
    if((sock = accept(*sock, (struct sockaddr*)&their_addr,&sin_size)) == -1)
    {
        printf("Error: accept()\n");
        return -1;
    }

}