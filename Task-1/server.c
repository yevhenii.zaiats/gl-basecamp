#include "func.h"

int main()
{
    //Variables
    int listen_sock, sock; //For socket
    int numb;
    char buf[MAXLEN];
    struct addrinfo hints, *servinfo; 
    struct sockaddr_storage their_addr;
    socklen_t sin_size;
    int yes = 1;
    char s[INET6_ADDRSTRLEN];
    //Fill struct data
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if((getaddrinfo(NULL,PORT,&hints,&servinfo)) != 0)
    {
        printf("Error : getadrrinfo()\n");
        return -1;
    }
    //Create socket
    if((listen_sock = socket(servinfo->ai_family,servinfo->ai_socktype, servinfo->ai_protocol))==-1)
    {
        printf("Error: socket()\n");
        return -1;
    }
    if(setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        {
            printf("Error: setsockopt\n");
            return -1;
        }
    //Bind socket
    if((bind(listen_sock, servinfo->ai_addr, servinfo->ai_addrlen)) == -1)
    {
        close(listen_sock);
        printf("Error: bind()\n");
        return -1;
    }
    freeaddrinfo(servinfo);
    //Listening
    if((listen(listen_sock,BACKLOG)) == -1)
    {
        printf("Error: Listen()\n");
        return -1;
    }
    printf("Server waiting for connection ... ... ...\n");
    //Accept
    sin_size = sizeof(their_addr);
    if((sock = accept(listen_sock, (struct sockaddr*)&their_addr,&sin_size)) == -1)
    {
        printf("Error: accept()\n");
        return -1;
    }
    //Show our connection
    inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr*)&their_addr), s, sizeof(s));
    printf("Server connected with %s \n",s);
    while(1)
    {
        
        memset(&buf, 0, MAXLEN);
        //Send message
        if((send(sock, "Succesfull connection!", sizeof("Succesfull connection!"), 0)) == -1)
        {
            printf("Error: send()\n");
            continue;
        }
        if((numb = recv(sock, buf, MAXLEN, 0)) == -1)
        {
            printf("Error: recv()\n");
            continue;
        }
        if(numb == 0)
        {
            printf("Client disconected!\n");
            break;
        }
        printf("%s", buf);
    }
    close(sock);
    return 0;
}